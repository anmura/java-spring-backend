insert into USERS (username, password, enabled, first_name) values ('user', '$2a$12$dIt7Gt0vpDEuRJJhjJ553esjcq7EbRF0UA96.T3t5H1h8Z/hy7K72', true, 'user');
insert into USERS (username, password, enabled, first_name) values ('admin', '$2a$12$QsKK4IcIJizIyZFSccZV0uQ4ghHcD9SIB4tIpgtKkoBy2gXYjOpnO', true, 'admin');

insert into AUTHORITIES values ('user', 'USER');
insert into AUTHORITIES values ('admin', 'USER');
insert into AUTHORITIES values ('admin', 'ADMIN')