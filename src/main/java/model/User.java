package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@RequiredArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @NotNull
    @Id
    private String username;

    @NotNull
    private String password;

    @NotNull
    private boolean enabled;

    @NotNull
    @Column(name = "first_name")
    private String firstName;
//
//    @ElementCollection(fetch = FetchType.EAGER)
//    @CollectionTable(
//            name = "AUTHORITIES",
//            joinColumns = @JoinColumn(name = "username",
//                    referencedColumnName = "usernameId")
//    )
//    private List<Authority> authorities;

//    public User(String username, String password, boolean b, String firstName) {
//        this.username = username;
//        this.password = password;
//        this.enabled = b;
//        this.firstName = firstName;
//    }
}