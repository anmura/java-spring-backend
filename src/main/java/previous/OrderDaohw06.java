//package previous;
//
//import model.Item;
//import model.Order;
//
//import javax.sql.DataSource;
//import java.sql.*;
//import java.util.ArrayList;
//import java.util.List;
//
//public class OrderDaohw06 {
//
//    private DataSource dataSource;
//
//    public OrderDaohw06(DataSource dataSource) {
//
//        this.dataSource = dataSource;
//    }
//
//
//    public List<Order> getAllOrders(boolean delay) {
//        String sql = "select * from \"order\" o LEFT JOIN \"item\" i ON o.id = i.orderId";
//
//        try (Connection conn = dataSource.getConnection();
//             Statement stmt = conn.createStatement()) {
//
//            if (delay) {
//                Thread.sleep(1000);
//            }
//
//            ResultSet rs = stmt.executeQuery(sql);
//
//            List<Order> orders = new ArrayList<>();
//
//            while (rs.next()) {
//                Order nextOrder = new Order();
//                nextOrder.setId(rs.getLong("id"));
//                nextOrder.setOrderNumber(rs.getString("orderNumber"));
//
//                Item item = new Item();
//                item.setItemName(rs.getString("itemName"));
//                item.setPrice(rs.getInt("price"));
//                item.setQuantity(rs.getInt("quantity"));
//                if (!orders.isEmpty() && rs.getLong("id") == orders.get(orders.size() - 1).getId()) {
//                    Order order = orders.remove(orders.size() - 1);
//                    List<Item> items = order.getOrderRows();
//                    items.add(item);
//                    orders.add(order);
//                } else {
//                    List<Item> orderRows = new ArrayList<>();
//                    orderRows.add(item);
//                    nextOrder.setOrderRows(orderRows);
//                    orders.add(nextOrder);
//                }
//            }
//            return orders;
//
//        } catch (SQLException | InterruptedException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    public Order getOrderById(Long id) {
//        String sql = "select * from \"order\" o LEFT JOIN \"item\" i ON o.id = i.orderId where o.id = ?";
//
//        try (Connection conn = dataSource.getConnection();
//             PreparedStatement ps = conn.prepareStatement(sql)) {
//            ps.setLong(1, id);
//
//            ResultSet rs = ps.executeQuery();
//
//            List<Item> orderRows = new ArrayList<>();
//            Order order = new Order();
//
//            while (rs.next()) {
//                order.setId(rs.getLong("id"));
//                order.setOrderNumber(rs.getString("orderNumber"));
//
//
//                Item item = new Item();
//                item.setItemName(rs.getString("itemName"));
//                item.setPrice(rs.getInt("price"));
//                item.setQuantity(rs.getInt("quantity"));
//
//                orderRows.add(item);
//            }
//            order.setOrderRows(orderRows);
//
//            return order;
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    public Order saveOrder(Order order) {
//
//        String sql = "insert into \"order\" (orderNumber) values (?)";
//
//        try (Connection conn = dataSource.getConnection();
//             PreparedStatement ps = conn.prepareStatement(sql, new String[]{"id"})) {
//
//            ps.setString(1, order.getOrderNumber());
//
//            ps.executeUpdate();
//
//            ResultSet rs = ps.getGeneratedKeys();
//
//            if (!rs.next()) {
//                throw new RuntimeException("unexpected");
//            }
//
//            saveItems(order.getOrderRows(), rs.getLong("id"));
//
//            return new Order(rs.getLong("id"), order.getOrderNumber(), order.getOrderRows());
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//
//    }
//
//    private void saveItems(List<Item> items, long orderId) {
//
//        String sql = "insert into \"item\" (itemName, quantity, price, orderId) values (?,?,?,?)";
//
//        try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
//
//            conn.setAutoCommit(false);
//
//            for (Item item : items) {
//
//                ps.setString(1, item.getItemName());
//                ps.setInt(2, item.getQuantity());
//                ps.setInt(3, item.getPrice());
//                ps.setLong(4, orderId);
//
//                ps.addBatch();
//            }
//
//            ps.executeBatch();
//            conn.commit();
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    public void deleteOrderById(long id) {
//        String sql = "DELETE FROM \"order\" WHERE id = ?";
//
//        try (Connection conn = dataSource.getConnection();
//             PreparedStatement ps = conn.prepareStatement(sql)) {
//            ps.setLong(1, id);
//
//            ps.executeQuery();
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//
//    }
//
//    public List<Long> saveOrders(List<Order> orders) {
//
//        String orderSql = "insert into \"order\" (orderNumber) values (?)";
//
//        List<Long> idList = new ArrayList<>();
//
//        try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(orderSql, new String[]{"id"})) {
//
//            conn.setAutoCommit(false);
//
//            for (Order order : orders) {
//
//                ps.setString(1, order.getOrderNumber());
//
//                ps.addBatch();
//
//            }
//            ps.executeBatch();
//
//            ResultSet rs = ps.getGeneratedKeys();
//
//            while (rs.next()){
//                long orderId = rs.getLong("id");
//                idList.add(orderId);
//            }
//
//            conn.commit();
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//
//        String itemSql = "insert into \"item\" (itemName, quantity, price, orderId) values (?,?,?,?)";
//
//        try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(itemSql)) {
//
//            conn.setAutoCommit(false);
//
//            for (int i = 0; i < orders.size(); i++) {
//
//                for (Item item : orders.get(i).getOrderRows()) {
//
//                    ps.setString(1, item.getItemName());
//                    ps.setInt(2, item.getQuantity());
//                    ps.setInt(3, item.getPrice());
//                    ps.setLong(4, idList.get(i));
//
//                    ps.addBatch();
//                }
//            }
//
//            ps.executeBatch();
//            conn.commit();
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//
//        return idList;
//    }
//}
