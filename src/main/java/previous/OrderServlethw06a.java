// hw06a - post with list
//package previous;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.SneakyThrows;
//import model.Order;
//import util.Util;
//
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
////@WebServlet
//public class OrderServlet extends HttpServlet {
//
//    @SneakyThrows
//    @Override
//    public void doGet(HttpServletRequest request,
//                      HttpServletResponse response) {
//
//        String id = request.getParameter("id");
//
//        OrderDao orderDao = (OrderDao) getServletContext().getAttribute("orderDao");
//
//        if (id == null) {
//
//            List<Order> orders = orderDao.getAllOrders(false);
//            String jsonOrders = new ObjectMapper().writeValueAsString(orders);
                //or String json = request.getReader().lines().collect(Collectors.joining("\n"));
//
//            response.getWriter().println(jsonOrders);
//
//        } else {
//
//            Order order = orderDao.getOrderById(Long.parseLong(id));
//            String jsonOrder = new ObjectMapper().writeValueAsString(order);
//
//            response.getWriter().println(jsonOrder);
//
//        }
//        response.setContentType("application/json");
//
//    }
//
//    @SneakyThrows
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse resp) {
//
//        OrderDao orderDao = (OrderDao) getServletContext().getAttribute("orderDao");
//
//        resp.setContentType("application/json");
//
//        String json = Util.readStream(request.getInputStream());
//
//        if (request.getRequestURI().contains("bulk")) {
//
//            List<Order> orders = new ObjectMapper().readValue(json, new TypeReference<List<Order>>(){});
//            List<Long> ordersId = orderDao.saveOrders(orders);
//
//            String jsonOrdersId = ordersId.toString();
//            resp.getWriter().println(jsonOrdersId);
//
//        }else {
//
//
//            Order order = new ObjectMapper().readValue(json, Order.class);
//
//            Order newOrder = orderDao.saveOrder(order);
//
//
//            String jsonOrder = new ObjectMapper().writeValueAsString(newOrder);
//
//            resp.getWriter().println(jsonOrder);
//        }
//
//    }
//
//    protected void doDelete(HttpServletRequest request, HttpServletResponse response) {
//
//        String id = request.getParameter("id");
//
//        OrderDao orderDao = (OrderDao) getServletContext().getAttribute("orderDao");
//        if (id != null) {
//            orderDao.deleteOrderById(Long.parseLong(id));
//
//        }
//    }
//}