//package previous;
//
//import lombok.SneakyThrows;
//import util.ConfigUtil;
//import util.ConnectionInfo;
//import util.ConnectionPoolFactory;
//import util.FileUtil;
//
//import javax.servlet.ServletContext;
//import javax.servlet.ServletContextEvent;
//import javax.servlet.ServletContextListener;
//import javax.servlet.annotation.WebListener;
//import javax.sql.DataSource;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//import java.sql.Statement;
//
//@WebListener
//public class Listenerhw06a implements ServletContextListener {
//
//    @SneakyThrows
//    @Override
//    public void contextInitialized(ServletContextEvent sce) {
//        createSchema();
//
//
//        ServletContext context = sce.getServletContext();
//
//        DataSource pool = new ConnectionPoolFactory().createConnectionPool();
//        OrderDao orderDao = new OrderDao(pool);
//
//        context.setAttribute("pool", pool);
//        context.setAttribute("orderDao", orderDao);
//
//        var reg = context.addServlet("OrderServlet", new OrderServlet());
//        reg.addMapping("/api/orders", "/api/orders/bulk");
//
//    }
//
//    @Override
//    public void contextDestroyed(ServletContextEvent sce) {
//
//    }
//
//
//    private static void createSchema() throws SQLException {
//        ConnectionInfo connectionInfo = ConfigUtil.readConnectionInfo();
//
//        Connection conn = DriverManager.getConnection(
//                connectionInfo.getUrl(),
//                connectionInfo.getUser(),
//                connectionInfo.getPass()
//        );
//
//        try (conn; Statement stmt = conn.createStatement()) {
//            String schema = FileUtil.readFileFromClasspath("schema.sql");
//
//            stmt.executeUpdate(schema);
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//
//    }
//}
