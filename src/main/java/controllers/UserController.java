package controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
public class UserController {

    @GetMapping("users/{userName}")
    @PreAuthorize("#userName == authentication.name or hasAuthority('ADMIN')")
    public String getUser(@PathVariable String userName, Authentication auth) {
        return "user: " + auth;
    }

    @GetMapping("users")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String users(Authentication auth) {
        return "user: " + auth;
    }


//    @PostMapping("LINK")
//    public String NAME(@RequestBody TYPE NAME) {
//      ...
//    }

}