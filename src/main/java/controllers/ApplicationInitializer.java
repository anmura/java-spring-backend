package controllers;

import config.HsqlDataSource;
import config.DbConfig;
import config.PostgresDataSource;
import config.SecurityConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class ApplicationInitializer extends
        AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/api/*"};
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] {DbConfig.class, HsqlDataSource.class, SecurityConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{};
    }
}
