package controllers;

import Dao.OrderDao;
import model.Order;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {
    private final OrderDao orderDao;

    public OrderController(OrderDao dao) {
        this.orderDao = dao;
    }

    @PostMapping("orders")
    public Order saveOrder(@RequestBody @Valid Order order) {
        return orderDao.saveOrder(order);
    }

    @GetMapping("orders")
    public List<Order> getAllOrders() {
        return orderDao.getAllOrders();
    }

    @GetMapping("orders/{id}")
    public Order getOrderById(@PathVariable Long id) {
        return orderDao.getOrderById(id);
    }

    @DeleteMapping("orders/{id}")
    public void deleteOrderById(@PathVariable Long id) {
        orderDao.deleteOrderById(id);
    }

    @GetMapping("version")
    public void version() {
    }

}
