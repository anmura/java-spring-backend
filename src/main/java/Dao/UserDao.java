package Dao;

import model.Authority;
import model.Order;
import model.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Convert;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void saveUser(User user, String... authorities) {

        em.persist(user);
        for (String auth: authorities){
            em.persist(new Authority(user.getUsername(), "ROLE_" + auth));
        }

    }

}
